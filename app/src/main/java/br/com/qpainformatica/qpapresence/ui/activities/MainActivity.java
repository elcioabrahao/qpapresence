package br.com.qpainformatica.qpapresence.ui.activities;

import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;

import br.com.qpainformatica.qpabeacon.domain.model.SampleBeacon;
import br.com.qpainformatica.qpabeacon.ui.activity.BeaconActivity;
import br.com.qpainformatica.qpapresence.R;
import br.com.qpainformatica.qpapresence.domain.events.ListUpdatedEvent;
import br.com.qpainformatica.qpapresence.domain.model.EventoContent;
import br.com.qpainformatica.qpapresence.domain.model.Presenca;
import br.com.qpainformatica.qpapresence.ui.fragments.CadastroFragment;
import br.com.qpainformatica.qpapresence.ui.fragments.DevideBillFragment;
import br.com.qpainformatica.qpapresence.ui.fragments.EventoFragment;
import br.com.qpainformatica.qpapresence.ui.fragments.PresencaFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BeaconActivity implements PresencaFragment.OnListFragmentInteractionListener, EventoFragment.OnEventoListFragmentInteractionListener {

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.toolbar_title)
    TextView textViewToolBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AppBarLayout barLayout = (AppBarLayout) findViewById(R.id.barlayout);

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add("Page A", EventoFragment.class)
                .add("Page B", PresencaFragment.class)
                .create());


        SmartTabLayout viewPagerTab = (SmartTabLayout) barLayout.findViewById(R.id.viewpagertab);

        final LayoutInflater inflater = LayoutInflater.from(viewPagerTab.getContext());
        final Resources res = viewPagerTab.getContext().getResources();


        viewPagerTab.setCustomTabView(new SmartTabLayout.TabProvider() {
            @Override
            public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
                ImageView icon = (ImageView) inflater.inflate(R.layout.custom_tab_icon, container, false);
                switch (position) {
                    case 0:
                        icon.setImageDrawable(res.getDrawable(R.drawable.ic_school_white_24dp));
                        break;
                    case 1:
                        icon.setImageDrawable(res.getDrawable(R.drawable.ic_ibeacon_white_24dp));
                        break;
                    default:
                        throw new IllegalStateException("Invalid position: " + position);
                }
                return icon;
            }
        });

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
                        textViewToolBarTitle.setText("Eventos");
                        break;
                    case 1:
                        textViewToolBarTitle.setText("Presenças");
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPagerTab.setViewPager(viewPager);





        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Presenca.deleteAll(Presenca.class);
                EventBus.getDefault().post(new ListUpdatedEvent("List deleted"));

                return false;
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                boolean achou = false;
//                Log.d("BEACONS","#####################################");
//                Presenca presenca;
//
//                for(SampleBeacon beacon: mBeaconItems){
//                    achou = true;
//                    String id = ""+beacon.id;
//                    String evento="Evento não identificado";
//                    if(id.equalsIgnoreCase("006c89c9")){
//                        evento="Palestra Prof. Elcio";
//                    }else if(id.equalsIgnoreCase("006c89cb")){
//                        evento="Evento Startups";
//                    }else if(id.equalsIgnoreCase("006c89ca")){
//                        evento="Aula Lógica Programação";
//                    }
//
//
//
//                    presenca = new Presenca(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(beacon.lastDetectedTimestamp),evento,""+beacon.id);
//                    presenca.save();
//                    Log.d("BEACONS","address:"+beacon.deviceAddress);
//                    Log.d("BEACONS","id     :"+beacon.id);
//                    Log.d("BEACONS","time   :"+beacon.lastDetectedTimestamp);
//                    Log.d("BEACONS","rssi   :"+beacon.latestRssi);
//                    Log.d("BEACONS","--------------------------------------");
//
//                }
//                if(!achou){
//                    Snackbar.make(view, "Nenhuma presença localizada!", Snackbar.LENGTH_SHORT)
//                            .setAction("Action", null).show();
//                }else{
//                    EventBus.getDefault().post(new ListUpdatedEvent("List updated"));
//                }

            }
        });
    }


    @Override
    public void onListFragmentInteraction(Presenca item) {

    }


    @Override
    public void onListFragmentInteraction(EventoContent.DummyItem item) {

    }
}
