package br.com.qpainformatica.qpapresence.ui.fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.qpainformatica.qpapresence.R;
import br.com.qpainformatica.qpapresence.ui.fragments.PresencaFragment.OnListFragmentInteractionListener;
import br.com.qpainformatica.qpapresence.domain.model.Presenca;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Presenca} and makes a call to the
 * specified
 * TODO: Replace the implementation with code for your data type.
 */
public class PresencaRecyclerViewAdapter extends RecyclerView.Adapter<PresencaRecyclerViewAdapter.ViewHolder> {

    private  List<Presenca> mValues;
    private  OnListFragmentInteractionListener mListener;

    public PresencaRecyclerViewAdapter(List<Presenca> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_presenca, parent, false);
        return new ViewHolder(view);
    }

    public void updateList(List<Presenca> items, OnListFragmentInteractionListener listener){
        mValues = items;
        mListener = listener;
        this.notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getData());
        //holder.mContentView.setText(mValues.get(position).getEvento());
        holder.mContentView.setText("");

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public Presenca mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
