package br.com.qpainformatica.qpapresence.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.vstechlab.easyfonts.EasyFonts;

import br.com.qpainformatica.qpapresence.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoActivity extends Activity  {


    private TextureVideoView mTextureVideoView;

    @BindView(R.id.text1)
    TextView text1;
    @BindView(R.id.text2)
    TextView text2;
    @BindView(R.id.text3)
    TextView text3;
    @BindView(R.id.text4)
    TextView text4;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);

        text1.setTypeface(EasyFonts.robotoBlackItalic(this));
        text2.setTypeface(EasyFonts.robotoBold(this));
        text3.setTypeface(EasyFonts.robotoBold(this));
        text4.setTypeface(EasyFonts.robotoBold(this));

        mTextureVideoView = (TextureVideoView)findViewById(R.id.cropTextureView);
        mTextureVideoView.setScaleType(TextureVideoView.ScaleType.CENTER_CROP);
        mTextureVideoView.setDataSource(getApplicationContext(), R.raw.intro5);
        mTextureVideoView.play();
        mTextureVideoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(VideoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }


}