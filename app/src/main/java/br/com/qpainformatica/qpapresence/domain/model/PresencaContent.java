package br.com.qpainformatica.qpapresence.domain.model;

import java.util.List;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class PresencaContent {

   public static List<Presenca> getPresencaList(){

       return Presenca.findWithQuery(Presenca.class, "Select * from Presenca order by id desc");

   }

}
