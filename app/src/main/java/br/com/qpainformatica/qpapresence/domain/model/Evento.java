package br.com.qpainformatica.qpapresence.domain.model;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * Created by elcio on 26/09/16.
 */

public class Evento extends SugarRecord implements Serializable {

    //teste

    private long remoteEventId;
    private String nome;
    private String descricao;
    private String url;
    private String image;
    private String identificador1;
    private String identificador2;
    private String identificador3;
    private String organizador;
    private String logradouro;
    private String numero;
    private String endereco;
    private String cidade;
    private String cep;
    private String estado;
    private String dataInicio;
    private String dataTermino;
    private String dataLimiteInscricao;
    private double valorIngresso;
    private String beaconsIds;

    public Evento(){}

    public String getBeaconsIds() {
        return beaconsIds;
    }

    public void setBeaconsIds(String beaconsIds) {
        this.beaconsIds = beaconsIds;
    }

    public long getRemoteEventId() {
        return remoteEventId;
    }

    public void setRemoteEventId(long remoteEventId) {
        this.remoteEventId = remoteEventId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIdentificador1() {
        return identificador1;
    }

    public void setIdentificador1(String identificador1) {
        this.identificador1 = identificador1;
    }

    public String getIdentificador3() {
        return identificador3;
    }

    public void setIdentificador3(String identificador3) {
        this.identificador3 = identificador3;
    }

    public String getIdentificador2() {
        return identificador2;
    }

    public void setIdentificador2(String identificador2) {
        this.identificador2 = identificador2;
    }

    public String getOrganizador() {
        return organizador;
    }

    public void setOrganizador(String organizador) {
        this.organizador = organizador;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataTermino() {
        return dataTermino;
    }

    public void setDataTermino(String dataTermino) {
        this.dataTermino = dataTermino;
    }

    public String getDataLimiteInscricao() {
        return dataLimiteInscricao;
    }

    public void setDataLimiteInscricao(String dataLimiteInscricao) {
        this.dataLimiteInscricao = dataLimiteInscricao;
    }

    public double getValorIngresso() {
        return valorIngresso;
    }

    public void setValorIngresso(double valorIngresso) {
        this.valorIngresso = valorIngresso;
    }
}
