package br.com.qpainformatica.qpapresence;

import com.orm.SugarApp;

import br.com.qpainformatica.qpapresence.domain.model.Presenca;

/**
 * Created by elcio on 17/06/16.
 */
public class PresenceApplication extends SugarApp {
    @Override
    public void onCreate() {
        super.onCreate();

        Presenca.findById(Presenca.class, (long) 1);
    }
}
