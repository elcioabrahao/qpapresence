package br.com.qpainformatica.qpapresence.domain.model;

import com.orm.SugarRecord;

/**
 * Created by elcio on 10/06/16.
 */
public class Presenca extends SugarRecord {

    String data;
    String remoteEventId;
    String deviceUniqId;
    String identificador1;
    String identificador2;
    String identificador3;
    String eventoConfirmationToken;


    public Presenca(){

    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getRemoteEventId() {
        return remoteEventId;
    }

    public void setRemoteEventId(String remoteEventId) {
        this.remoteEventId = remoteEventId;
    }

    public String getDeviceUniqId() {
        return deviceUniqId;
    }

    public void setDeviceUniqId(String deviceUniqId) {
        this.deviceUniqId = deviceUniqId;
    }

    public String getIdentificador1() {
        return identificador1;
    }

    public void setIdentificador1(String identificador1) {
        this.identificador1 = identificador1;
    }

    public String getIdentificador3() {
        return identificador3;
    }

    public void setIdentificador3(String identificador3) {
        this.identificador3 = identificador3;
    }

    public String getIdentificador2() {
        return identificador2;
    }

    public void setIdentificador2(String identificador2) {
        this.identificador2 = identificador2;
    }

    public String getEventoConfirmationToken() {
        return eventoConfirmationToken;
    }

    public void setEventoConfirmationToken(String eventoConfirmationToken) {
        this.eventoConfirmationToken = eventoConfirmationToken;
    }
}
