package br.com.qpainformatica.qpapresence.domain.events;

/**
 * Created by elcio on 17/06/16.
 */
public class ListUpdatedEvent {

    public final String message;

    public ListUpdatedEvent(String message){
        this.message=message;
    }

}
